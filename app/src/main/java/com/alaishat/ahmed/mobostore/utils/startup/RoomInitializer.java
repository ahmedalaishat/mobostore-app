package com.alaishat.ahmed.mobostore.utils.startup;

import android.content.Context;

import com.alaishat.ahmed.mobostore.model.datasource.room.MoboDatabase;
import com.alaishat.ahmed.mobostore.model.repositories.UserRepository;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.startup.Initializer;

public class RoomInitializer implements Initializer<MoboDatabase> {
    @NonNull
    @Override
    public MoboDatabase create(@NonNull Context context) {
        return MoboDatabase.init(context);
    }

    @NonNull
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return Collections.emptyList();
    }
}
