package com.alaishat.ahmed.mobostore.model.datasource.retrofit.res;

import com.alaishat.ahmed.mobostore.model.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("user")
    @Expose
    private User user;

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }
}
