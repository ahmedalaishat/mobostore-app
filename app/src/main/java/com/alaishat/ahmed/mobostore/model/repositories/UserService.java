package com.alaishat.ahmed.mobostore.model.repositories;

import com.alaishat.ahmed.mobostore.model.datasource.retrofit.res.UserResponse;
import com.alaishat.ahmed.mobostore.model.datasource.retrofit.req.SignInReqBody;
import com.alaishat.ahmed.mobostore.model.datasource.retrofit.req.SignUpReqBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    @POST("/users/sign-in")
    Call<UserResponse> signIn(@Body SignInReqBody request);
    @POST("/users/sign-up")
    Call<UserResponse> signUp(@Body SignUpReqBody request);
}