package com.alaishat.ahmed.mobostore.model.repositories;

import com.alaishat.ahmed.mobostore.model.models.User;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface UserDoa {
    @Insert
    public void insertUser(User user);

    @Update
    public void updateUser(User user);

    @Delete
    public void deleteUser(User user);

    @Query("delete from user_table")
    public void delete();

    @Query("select * from user_table Limit 1")
    public User getUser();
}