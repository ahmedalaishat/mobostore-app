package com.alaishat.ahmed.mobostore.utils.startup;

import android.content.Context;

import com.alaishat.ahmed.mobostore.model.repositories.UserRepository;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.startup.Initializer;

public class UserInitializer implements Initializer<UserRepository> {
    @NonNull
    @Override
    public UserRepository create(@NonNull Context context) {
        return UserRepository.getInstance();
    }

    @NonNull
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return Arrays.asList(RoomInitializer.class);
    }
}
