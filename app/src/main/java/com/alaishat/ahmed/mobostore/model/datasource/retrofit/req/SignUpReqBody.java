package com.alaishat.ahmed.mobostore.model.datasource.retrofit.req;

public class SignUpReqBody {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String gender;
    private String bio;
    private String image;

    public SignUpReqBody(String username, String firstName, String lastName, String email, String password, String gender) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password=password;
        this.gender = gender;
    }
}
