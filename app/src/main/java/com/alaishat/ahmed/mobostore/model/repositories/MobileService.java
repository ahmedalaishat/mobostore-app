package com.alaishat.ahmed.mobostore.model.repositories;

import com.alaishat.ahmed.mobostore.model.models.Brand;
import com.alaishat.ahmed.mobostore.model.models.Mobile;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface MobileService {
    @GET("/mobiles")
    Call<ArrayList<Mobile>> indexMobile(@Header("Authorization") String authorization);

    @GET("/mobiles/{id}")
    Call<Mobile> showMobile(@Header("Authorization") String authorization, @Path("id") String id);
//    TODO search

    @GET("/brands")
    Call<ArrayList<Brand>> indexBrands(@Header("Authorization") String authorization);

    @GET("/brands/{id}")
    Call<Brand> showBrands(@Header("Authorization") String authorization, @Path("id") String id);
}
