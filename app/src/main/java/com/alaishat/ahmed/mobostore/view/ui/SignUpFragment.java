package com.alaishat.ahmed.mobostore.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alaishat.ahmed.mobostore.R;
import com.alaishat.ahmed.mobostore.databinding.FragmentSignInBinding;
import com.alaishat.ahmed.mobostore.databinding.FragmentSignUpBinding;
import com.alaishat.ahmed.mobostore.viewmodel.SignInViewModel;
import com.alaishat.ahmed.mobostore.viewmodel.SignUpViewModel;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

public class SignUpFragment extends Fragment implements Observer<String> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSignUpBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);
        SignUpViewModel signUpViewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        signUpViewModel.getResult().observe(getViewLifecycleOwner(), this);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setSignUpViewModel(signUpViewModel);
        binding.toSignIn.setOnClickListener((View view)->{
            Navigation.findNavController(view).navigate(R.id.action_sign_up_to_sign_in);
        });
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onChanged(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }
}