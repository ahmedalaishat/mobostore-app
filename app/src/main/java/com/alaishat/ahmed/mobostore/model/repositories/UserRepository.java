package com.alaishat.ahmed.mobostore.model.repositories;

import com.alaishat.ahmed.mobostore.model.datasource.retrofit.ServiceGenerator;
import com.alaishat.ahmed.mobostore.model.datasource.retrofit.req.SignInReqBody;
import com.alaishat.ahmed.mobostore.model.datasource.retrofit.req.SignUpReqBody;
import com.alaishat.ahmed.mobostore.model.datasource.retrofit.res.UserResponse;
import com.alaishat.ahmed.mobostore.model.datasource.room.MoboDatabase;
import com.alaishat.ahmed.mobostore.model.models.User;
import com.alaishat.ahmed.mobostore.utils.errs.ErrorUtils;
import com.alaishat.ahmed.mobostore.view.ui.callback.SignResultCallback;

import org.jetbrains.annotations.NotNull;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;

public class UserRepository {
    private static UserRepository instance;
    private UserDoa userDoa;
    private UserService userService;
    private MutableLiveData<User> userLiveData;

    public static UserRepository getInstance() {
        if (instance == null)
            instance = new UserRepository();
        return instance;
    }

    public String getAut() {
        return userLiveData.getValue() != null ? "bearer " + userLiveData.getValue().getToken() : "";
    }

    private UserRepository() {
        userLiveData = new MutableLiveData<>();
        MoboDatabase moboDatabase = MoboDatabase.getINSTANCE();
        userDoa = moboDatabase.userDao();
        MoboDatabase.databaseWriteExecutor.execute(() -> {
            userLiveData.postValue(userDoa.getUser());
        });
        userService = ServiceGenerator.createService(UserService.class);
    }

    public Callback<UserResponse> getSignCallback(SignResultCallback callback) {
        return new Callback<UserResponse>() {
            @Override
            public void onResponse
                    (@NotNull Call<UserResponse> call, @NotNull retrofit2.Response<UserResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MoboDatabase.databaseWriteExecutor.execute(() -> {
                        userDoa.delete();
                        userDoa.insertUser(response.body().getUser());
                        userLiveData.postValue(response.body().getUser());
                        callback.onSuccess(response.body().getMessage());
                    });
                } else
                    callback.onError(ErrorUtils.getError(response));
            }

            @Override
            public void onFailure(@NotNull Call<UserResponse> call, @NotNull Throwable t) {
                callback.onError(ErrorUtils.getError(t));
            }
        };
    }

    public void signOut(SignResultCallback callback) {
        if (userLiveData.getValue() == null)
            callback.onError("You are not logged in!.");
        else MoboDatabase.databaseWriteExecutor.execute(() -> {
            userLiveData.postValue(null);
            userDoa.deleteUser(userLiveData.getValue());
            callback.onSuccess("Sign out Successful!.");
        });
    }

    public void signIn(SignInReqBody request, SignResultCallback callback) {
        userService.signIn(request).enqueue(getSignCallback(callback));
    }

    public void signUp(SignUpReqBody request, SignResultCallback callback) {
        userService.signUp(request).enqueue(getSignCallback(callback));
    }

    public MutableLiveData<User> getUserLiveData() {
        return userLiveData;
    }
}