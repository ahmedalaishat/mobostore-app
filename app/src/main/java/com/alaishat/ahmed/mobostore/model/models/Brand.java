package com.alaishat.ahmed.mobostore.model.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Brand {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("numberOfMobiles")
    @Expose
    private int numberOfMobiles;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("mobiles")
    @Expose
    private ArrayList<String> mobiles;

    @SerializedName("newestMobiles")
    @Expose
    private ArrayList<Mobile> newestMobiles;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Brand? ((Brand) obj)._id.equals(_id): super.equals(obj);
    }

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfMobiles() {
        return numberOfMobiles;
    }

    public String getUrl() {
        return url;
    }

    public ArrayList<String> getMobiles() {
        return mobiles;
    }

    public ArrayList<Mobile> getNewestMobiles() {
        return newestMobiles;
    }

    public void setNewestMobiles(ArrayList<Mobile> newestMobiles) {
        this.newestMobiles = newestMobiles;
    }
}
