package com.alaishat.ahmed.mobostore.model.repositories;

import com.alaishat.ahmed.mobostore.model.models.Brand;
import com.alaishat.ahmed.mobostore.model.models.Mobile;
import com.alaishat.ahmed.mobostore.model.datasource.retrofit.ServiceGenerator;
import com.alaishat.ahmed.mobostore.utils.errs.ErrorUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileRepository {
    private static MobileRepository instance;
    private MobileService mobileService;
    public ArrayList<Mobile> mobiles = new ArrayList<>();
    private ArrayList<Brand> brands = new ArrayList<>();
    private MutableLiveData<ArrayList<Mobile>> mobilesLiveData;
    private MutableLiveData<ArrayList<Brand>> brandsLiveData;


    public static MobileRepository getInstance(){
        if(instance==null)
            return new MobileRepository();
        return instance;
    }


    private MobileRepository() {
        mobilesLiveData = new MutableLiveData<>();
        mobileService = ServiceGenerator.createService(MobileService.class);
        brandsLiveData = new MutableLiveData<>();
    }

    public void indexMobiles() {
        mobileService.indexMobile(UserRepository.getInstance().getAut()).enqueue(new Callback<ArrayList<Mobile>>() {
            @Override
            public void onResponse(@NotNull Call<ArrayList<Mobile>> call, @NotNull Response<ArrayList<Mobile>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    handleMobileIndex(response.body());
                    mobilesLiveData.postValue(mobiles);
                } else
                    ErrorUtils.getError(response);
            }

            @Override
            public void onFailure(@NotNull Call<ArrayList<Mobile>> call, @NotNull Throwable t) {
                ErrorUtils.getError(t);
            }
        });
    }

    public void showMobile(Mobile mobile) {
//        TODO optimize mobile, get 10 from every brand
        mobileService.showMobile(UserRepository.getInstance().getAut(), mobile.get_id()).enqueue(new Callback<Mobile>() {
            @Override
            public void onResponse(@NotNull Call<Mobile> call, @NotNull Response<Mobile> response) {
                if (response.isSuccessful() && response.body() != null) {
                    handleMobile(mobile, response.body());
                    mobilesLiveData.postValue(mobiles);
                } else
                    ErrorUtils.getError(response);
            }

            @Override
            public void onFailure(@NotNull Call<Mobile> call, @NotNull Throwable t) {
                ErrorUtils.getError(t);
            }
        });
    }

    private void handleMobileIndex(ArrayList<Mobile> resMobiles) {
        if (mobiles.isEmpty())
            mobiles = resMobiles;
        else {
            for (Mobile mobile : mobiles) {
                if (mobiles.contains(mobile))
                    continue;
                mobiles.add(mobile);
            }
        }
    }

    private static void handleMobile(Mobile mobile, Mobile resMobile) {
        mobile.setQuickSpec(resMobile.getQuickSpec());
        mobile.setSpecDetail(resMobile.getSpecDetail());
    }

    public Mobile get(Mobile mobile1) {
        for (Mobile mobile : mobiles) {
            if (mobile.equals(mobile1))
                return mobile;
        }
        return null;
    }

    public void indexBrands() {
        mobileService.indexBrands(UserRepository.getInstance().getAut()).enqueue(new Callback<ArrayList<Brand>>() {
            @Override
            public void onResponse(@NotNull Call<ArrayList<Brand>> call, @NotNull Response<ArrayList<Brand>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    handleBrandsIndex(response.body());
                    brandsLiveData.postValue(brands);
                } else
                    ErrorUtils.getError(response);
            }

            @Override
            public void onFailure(@NotNull Call<ArrayList<Brand>> call, @NotNull Throwable t) {
                ErrorUtils.getError(t);
            }
        });
    }

    public void showBrand(Brand brand) {
        mobileService.showBrands(UserRepository.getInstance().getAut(), brand.get_id()).enqueue(new Callback<Brand>() {
            @Override
            public void onResponse(@NotNull Call<Brand> call, @NotNull Response<Brand> response) {
                if (response.isSuccessful() && response.body() != null) {
                    handleBrand(brand, response.body());
                    brandsLiveData.postValue(brands);
                } else
                    ErrorUtils.getError(response);
            }

            @Override
            public void onFailure(@NotNull Call<Brand> call, @NotNull Throwable t) {
                ErrorUtils.getError(t);
            }
        });
    }

    private void handleBrandsIndex(ArrayList<Brand> resBrands) {
        for (Brand resBrand : resBrands) {
            if (brands.contains(resBrand))
                continue;
            brands.add(resBrand);
        }
    }

    private void handleBrand(Brand brand, Brand resBrand) {
        ArrayList<Mobile> newestMobiles = new ArrayList<>();
        ArrayList<Mobile> resMobiles = resBrand.getNewestMobiles();
        ArrayList<Mobile> mobiles = this.mobiles;
        if (mobiles.isEmpty()) {
            this.mobiles = resMobiles;
            newestMobiles.addAll(resMobiles);
        } else
            for (Mobile resMobile : resMobiles) {
                if (mobiles.contains(resMobile))
                    newestMobiles.add(get(resMobile));
                else {
                    mobiles.add(resMobile);
                    newestMobiles.add(resMobile);
                }
            }
        brand.setNewestMobiles(newestMobiles);
    }
}
