package com.alaishat.ahmed.mobostore.utils.errs;

import com.alaishat.ahmed.mobostore.model.datasource.retrofit.ServiceGenerator;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {
    private static final String ERROR = "ERROR";

    public static APIError parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                ServiceGenerator.getRetrofit()
                        .responseBodyConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            assert response.errorBody() != null;
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }
        return error;
    }

    public static String getError(Response response) {
        assert response.errorBody() != null;
        APIError error = ErrorUtils.parseError(response);
        return error.getMessage();
    }

    public static String getError(Throwable t) {
        return "An error occurred: " + t.getMessage();
    }
}