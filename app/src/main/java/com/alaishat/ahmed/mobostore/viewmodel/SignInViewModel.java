package com.alaishat.ahmed.mobostore.viewmodel;

import com.alaishat.ahmed.mobostore.model.datasource.retrofit.req.SignInReqBody;
import com.alaishat.ahmed.mobostore.model.repositories.UserRepository;
import com.alaishat.ahmed.mobostore.view.ui.callback.SignResultCallback;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SignInViewModel extends ViewModel implements SignResultCallback {
    public MutableLiveData<String> username = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<String> result = new MutableLiveData<>();


    public MutableLiveData<String> getResult() {
        return result;
    }

    public void signIn() {
//        TODO validation
        SignInReqBody signInReqBody = new SignInReqBody(username.getValue() != null ? username.getValue() : "",
                password.getValue() != null ? password.getValue() : "");
        UserRepository.getInstance().signIn(signInReqBody, this);
    }

    @Override
    public void onSuccess(String message) {
        result.postValue(message);
    }

    @Override
    public void onError(String message) {
        result.postValue(message);
    }
}