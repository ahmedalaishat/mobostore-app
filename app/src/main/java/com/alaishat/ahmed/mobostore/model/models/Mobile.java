package com.alaishat.ahmed.mobostore.model.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mobile {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("img")
    @Expose
    private String img;

    @SerializedName("img_url")
    @Expose
    private String imgUrl;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("price")
    @Expose
    private String proce;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("quick_spec")
    @Expose
    private JsonObject quickSpec;

    @SerializedName("spec_detail")
    @Expose
    private JsonArray specDetail;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Mobile ? ((Mobile) obj)._id.equals(_id) : obj instanceof String ? obj.equals(_id) : super.equals(obj);
    }

    //todo is new
    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getImg() {
        return img;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public JsonObject getQuickSpec() {
        return quickSpec;
    }

    public JsonArray getSpecDetail() {
        return specDetail;
    }

    public void setQuickSpec(JsonObject quickSpec) {
        this.quickSpec = quickSpec;
    }

    public void setSpecDetail(JsonArray specDetail) {
        this.specDetail = specDetail;
    }
}
