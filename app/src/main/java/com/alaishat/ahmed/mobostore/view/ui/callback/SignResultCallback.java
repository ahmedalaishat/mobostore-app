package com.alaishat.ahmed.mobostore.view.ui.callback;

public interface SignResultCallback {
    public void onSuccess(String message);
    public void onError(String message);
}
