package com.alaishat.ahmed.mobostore.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alaishat.ahmed.mobostore.R;
import com.alaishat.ahmed.mobostore.databinding.FragmentSignInBinding;
import com.alaishat.ahmed.mobostore.viewmodel.SignInViewModel;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

public class SignInFragment extends Fragment implements Observer<String> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSignInBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false);
        SignInViewModel signInViewModel = new ViewModelProvider(this).get(SignInViewModel.class);
        signInViewModel.getResult().observe(getViewLifecycleOwner(), this);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setSignInViewModel(signInViewModel);
        binding.toSignUp.setOnClickListener((View view)->{
            Navigation.findNavController(view).navigate(R.id.action_sign_in_to_sign_up);
        });
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onChanged(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }
}