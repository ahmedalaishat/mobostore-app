package com.alaishat.ahmed.mobostore.model.datasource.room;

import android.content.Context;

import com.alaishat.ahmed.mobostore.model.models.User;
import com.alaishat.ahmed.mobostore.model.repositories.UserDoa;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class MoboDatabase extends RoomDatabase {
    private static volatile MoboDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public abstract UserDoa userDao();


    public static MoboDatabase getINSTANCE() {
        return INSTANCE;
    }

    public static MoboDatabase init(Context ctx) {
        if (INSTANCE == null) {
            synchronized (MoboDatabase.class) {
                INSTANCE = Room.databaseBuilder(ctx.getApplicationContext(),
                        MoboDatabase.class, "mobo-database")
                        .build();
            }
        }
        return INSTANCE;
    }
}
