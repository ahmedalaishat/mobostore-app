package com.alaishat.ahmed.mobostore.viewmodel;

import com.alaishat.ahmed.mobostore.model.datasource.retrofit.req.SignUpReqBody;
import com.alaishat.ahmed.mobostore.model.repositories.UserRepository;
import com.alaishat.ahmed.mobostore.view.ui.callback.SignResultCallback;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SignUpViewModel extends ViewModel implements SignResultCallback {
    public MutableLiveData<String> username = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<String> confirmPassword = new MutableLiveData<>();
    public MutableLiveData<String> firstName = new MutableLiveData<>();
    public MutableLiveData<String> lastName = new MutableLiveData<>();
    public MutableLiveData<String> email = new MutableLiveData<>();
    public MutableLiveData<String> gender = new MutableLiveData<>();

    private MutableLiveData<String> result =new MutableLiveData<>();

    public MutableLiveData<String> getResult() {
        return result;
    }

    public void signUp() {
//        TODO validation
//         add gender
        SignUpReqBody signUpReqBody = new SignUpReqBody(username.getValue() != null ? username.getValue().trim() : "",
                username.getValue() != null ? firstName.getValue().trim() : "", lastName.getValue() != null ? lastName.getValue() : "",
                email.getValue() != null ? email.getValue() : "", password.getValue() != null ? password.getValue() : "", "M");
        UserRepository.getInstance().signUp(signUpReqBody,this);
    }

    @Override
    public void onSuccess(String message) {
        result.postValue(message);
    }

    @Override
    public void onError(String message) {
        result.postValue(message);
    }
}