package com.alaishat.ahmed.mobostore.model.datasource.retrofit.req;

public class SignInReqBody {
    private String username;
    private String password;

    public SignInReqBody(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public SignInReqBody() {
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
