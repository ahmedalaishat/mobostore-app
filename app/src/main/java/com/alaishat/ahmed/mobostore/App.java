package com.alaishat.ahmed.mobostore;

import android.app.Application;

import com.alaishat.ahmed.mobostore.model.repositories.MobileRepository;
import com.alaishat.ahmed.mobostore.model.repositories.UserRepository;
import com.alaishat.ahmed.mobostore.utils.errs.ErrorUtils;

public class App extends Application {
    private static App single_instance = null;
    private UserRepository userRepository;
    private MobileRepository mobileRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        single_instance = this;
    }

    public static App getInstance() {
//        mustn't be null
        if (single_instance == null)
            single_instance = new App();
        return single_instance;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public MobileRepository getMobileRepository() {
        return mobileRepository;
    }
}
