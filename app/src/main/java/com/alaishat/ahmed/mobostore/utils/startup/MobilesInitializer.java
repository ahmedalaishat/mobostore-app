package com.alaishat.ahmed.mobostore.utils.startup;

import android.content.Context;

import com.alaishat.ahmed.mobostore.model.repositories.MobileRepository;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.startup.Initializer;

public class MobilesInitializer implements Initializer<MobileRepository> {
    @NonNull
    @Override
    public MobileRepository create(@NonNull Context context) {
        return MobileRepository.getInstance();
    }

    @NonNull
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return Arrays.asList(UserInitializer.class);
    }
}
